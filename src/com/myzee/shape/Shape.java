package com.myzee.shape;

public interface Shape {
	public void draw();
}
