package com.myzee.factorystore;

import java.awt.Rectangle;

import com.myzee.exception.InvalidShapeException;
import com.myzee.shape.Circle;
import com.myzee.shape.RectangleShape;
import com.myzee.shape.Shape;
import com.myzee.shape.Triangle;

public class ShapeFactory {
	public Shape getShape( String shapeType) {
		if (shapeType.equalsIgnoreCase("circle")) {
			return new Circle();
		} else if (shapeType.equalsIgnoreCase("rectangle")) {
			return new RectangleShape();
		} else if (shapeType.equalsIgnoreCase("triangle")) {
			return new Triangle();
		} else {
			throw new InvalidShapeException("Not a valid shape");
			
		}
	}
}
