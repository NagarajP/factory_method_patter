package com.myzee.main;

import com.myzee.exception.InvalidShapeException;
import com.myzee.factorystore.ShapeFactory;
import com.myzee.shape.Circle;
import com.myzee.shape.RectangleShape;
import com.myzee.shape.Shape;

public class FactoryMethodPatternMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ShapeFactory shape = new ShapeFactory();
		Shape c = shape.getShape("circle");
		c.draw();
		
		try {
			Shape s = shape.getShape("diamond");
			s.draw();
		} catch(InvalidShapeException e) {
			e.printStackTrace();
		}
		
		RectangleShape s = (RectangleShape) shape.getShape("rectangle");
		s.draw();
		
		System.out.println("End Main()");	
	}
}
